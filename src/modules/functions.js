import {
  addVisitBtn,
  logInBtn,
  logOutBtn,
  url,
  mainIn,
  mainOut,
  footer,
  visitsList,
  titleInput,
  statusInput,
  urgencyInput,
  templateEmail,
  templatePassword,
} from "./variables.js";
import { Request } from "./request.js";
import { visitCardCardiologist } from "./visitCards/visitCardCardiologist.js";
import { visitCardTherapist } from "./visitCards/visitCardTherapist.js";
import { visitCardDentist } from "./visitCards/visitCardDentist.js";
import { LogInForm } from "./forms/loginForm.js";
import { Modal } from "./modal.js";

export function getToken() {
  return localStorage.getItem("token") ? localStorage.getItem("token") : null;
}

export async function login(e, thisLogin, thisPassword) {
  try {
    e.preventDefault();
    if (!thisLogin || !thisPassword) {
      return alert("Username or password may not be empty!");
    }
    const response = await fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        email: thisLogin,
        password: thisPassword,
      }),
    });

    const token = await response.text();
    if (token === "Incorrect username or password") {
      return alert(token);
    } else {
      const targetItem = e.target.closest(".login__submit");
      if (targetItem) document.querySelector(".modal-wrapper").remove();

      setToken(token);
      toggleButtons();
      footer.style.backgroundColor = "#0B4066";
      visitCardsInit();
    }
  } catch (error) {
    console.log(error);
  }
}

// Set token to Local storage (when user successful logged in)
export function setToken(token) {
  localStorage.setItem("token", token);
}

// Remove token from Local storage (when user logged out)
export function removeToken() {
  localStorage.removeItem("token");
}

// toggle header buttons
export function toggleButtons() {
  addVisitBtn.classList.toggle("hidden");
  logOutBtn.classList.toggle("hidden");
  logInBtn.classList.toggle("hidden");
  toggleStub();
}

export function toggleStub() {
  mainIn.classList.toggle("hidden");
  mainOut.classList.toggle("hidden");
}

const cardArr = [];
export function visitCardsInit() {
  visitsList.innerText = "";
  Request.getAllCards().then((visits) => {
    if (!visits.length) {
      visitsList.insertAdjacentHTML(
        "beforeend",
        `<div class="no-items">No visits found</div>`
      );
    }
    visits.forEach((visit) => {
      cardRender(visit);
    });
    return cardArr;
  });
}

export function cardRender(obj) {
  let visitCard = {};

  switch (obj.doctor) {
    case "cardiologist":
      visitCard = new visitCardCardiologist(
        obj.patientName,
        obj.doctor,
        obj.title,
        obj.description,
        obj.urgency,
        obj.id,
        obj.status,
        obj.arterialPressure,
        obj.bmi,
        obj.illnesses,
        obj.age
      );
      break;
    case "therapist":
      visitCard = new visitCardTherapist(
        obj.patientName,
        obj.doctor,
        obj.title,
        obj.description,
        obj.urgency,
        obj.id,
        obj.status,
        obj.age
      );
      break;
    case "dentist":
      visitCard = new visitCardDentist(
        obj.patientName,
        obj.doctor,
        obj.title,
        obj.description,
        obj.urgency,
        obj.id,
        obj.status,
        obj.lastVisitDate
      );
  }
  if (document.querySelector(".no-items"))
    document.querySelector(".no-items").remove();
  cardArr.push(visitCard);
  const htmlCard = visitCard.createHTMLCard();
  colorMark(htmlCard, visitCard.urgency, visitCard.status);
  visitsList.append(htmlCard);
  visitCard.deleteBtnInit(htmlCard);
  visitCard.cardInteractionsInit(htmlCard);
  dragAndDropInit(htmlCard);
}

//------------- Filter visits

// filter
export function filterCards() {
  let noItems = visitsList.querySelector(".no-items");
  if (noItems) {
    noItems.remove();
  }
  cardArr.forEach((card) => {
    // check if card is matches filter
    const titleMatch = card.title
      .toLowerCase()
      .includes(titleInput.value.toLowerCase());
    const descrMatch = card.description
      .toLowerCase()
      .includes(titleInput.value.toLowerCase());
    const statusMatch =
      statusInput.value === "" ||
      statusInput.value === "all" ||
      card.status.toLowerCase() === statusInput.value.toLowerCase();
    const urgencyMatch =
      urgencyInput.value === "" ||
      urgencyInput.value === "all" ||
      card.urgency.toLowerCase() === urgencyInput.value;
    const match = (titleMatch || descrMatch) && statusMatch && urgencyMatch;

    // if card is not match - hide it
    const cardElem = document.querySelector(`[data-id="${card.id}"]`);
    if (!match) {
      cardElem.classList.add("hidden");
    } else {
      cardElem.classList.remove("hidden");
    }
  });
  let allItems = visitsList.querySelectorAll(".card-item:not(.hidden)");
  if (!allItems.length) {
    visitsList.insertAdjacentHTML(
      "afterbegin",
      `<div class="no-items">No visits found</div>`
    );
  }
}

// clear filter
export function clearFilter() {
  titleInput.value = "";
  statusInput.value = "";
  urgencyInput.value = "";
  filterCards();
}

export function colorMark(target, urgency, status) {
  if (status === "done") {
    return (target.style.borderLeft = "10px solid rgb(187 187 187)");
  }
  switch (urgency) {
    case "low":
      target.style.borderLeft = "10px solid #0f0";
      return;
    case "normal":
      target.style.borderLeft = "10px solid #FFFF00FF";
      return;
    case "high":
      target.style.borderLeft = "10px solid #f00";
      return;
  }
  return true;
}

export function validateFields(field, value) {
  if (field !== "description" && !value) return false;
  let pattern = "";
  switch (field) {
    case "patientName":
      pattern = /^[a-zA-Z]+\s[a-zA-Z]+$/;
      break;
    case "age":
      pattern = /^(1[8-9]|[2-9]\d)$/;
      break;
    case "arterialPressure":
      pattern = /^\d{2,3}\/\d{2,3}$/;
      break;
    case "bmi":
      pattern = /^\d+(\.\d+)?\/\d+(\.\d+)?$/;
      break;
    default:
      return true;
  }
  return pattern.test(value);
}

// ------ drag and drop functions block

let dragEl = "";
export function dragAndDropInit(card) {
  card.setAttribute("draggable", "true");
  card.addEventListener("dragstart", dragStart);
  card.addEventListener("dragend", dragEnd);
  card.addEventListener("dragenter", dragEnter);
  card.addEventListener("dragleave", dragLeave);
  card.addEventListener("dragover", dragOver);
  card.addEventListener("drop", dragDrop);
}

function dragDrop() {
  if (dragEl != this) {
    visitsList.insertBefore(dragEl, this);
  }
  return false;
}
function dragStart() {
  this.style.opacity = "0.4";
  dragEl = this;
}
function dragEnter() {
  this.classList.add("drag-hover");
  this.classList.add("over");
}
function dragEnd() {
  let listItems = document.querySelectorAll(".card-item");
  [].forEach.call(listItems, function (item) {
    item.classList.remove("over");
    item.classList.remove("drag-hover");
  });
  this.style.opacity = "1";
}

function dragLeave(event) {
  event.stopPropagation();
  this.classList.remove("over");
  this.classList.remove("drag-hover");
}

function dragOver(event) {
  event.preventDefault();
}

export function handleFormFilter(event) {
  if (event.key === "Escape") {
    clearFilter();
  }
  if (event.key === "Enter") {
    event.preventDefault();
  }
}

export function logInBtnClick() {
  const myLogin = new LogInForm();
  myLogin.createBaseForm();
  const loginEmail = myLogin.form.querySelector(".login__email");
  const loginPassword = myLogin.form.querySelector(".login__password");
  const loginSubmit = myLogin.form.querySelector(".login__submit");

  loginSubmit.addEventListener("click", (e) => {
    const thisLogin = loginEmail.value;
    const thisPassword = loginPassword.value;
    login(e, thisLogin, thisPassword);
  });
}

export function changeEditHTMLFields(event) {
  switch (event.currentTarget.value) {
    case "cardiologist":
      return ` <p>
                    <label for="age">Age</label>
                    <input placeholder='More than 18' class = 'input-base' type="number" name="age" value="${this.age}">
                </p>
                <p>
                    <label class="label-visit" for="arterial-pressure">Arterial Pressure</label>
                    <input placeholder='120/80' class = 'input-base' type="text" name="arterialPressure" value="${this.arterialPressure}">
                </p>
                <p>
                    <label for="bmi">BMI</label>
                    <input placeholder='Weigth / height(metres)' class = 'input-base' type="text" name="bmi" value="${this.bmi}">
                </p>
                <p>
                    <label for="illnesses">Illnesses</label>
                    <input class = 'input-base' type="text" name="illnesses" value="${this.illnesses}">
                </p>
                <div class="btn-wrapper"><button class = 'edit-form-btn' >Confirm changes</button></div>`;
    case "therapist":
      return `
                 <p>
                    <label for="age">Age</label>
                    <input placeholder='More than 18' class = 'input-base' type="number" name="age">
                </p>
                <div class="btn-wrapper"><button class = 'edit-form-btn'>Confirm changes</button></div>`;
    case "dentist":
      return ` <p>
                    <label for="last-visit">Last Visit</label>
                    <input max="${
                      new Date().toISOString().split("T")[0]
                    }" class = 'input-base' type="date" name="lastVisitDate" value="${
        this.lastVisitDate
      }">
                </p>
                <div class="btn-wrapper"><button class = 'edit-form-btn' >Confirm changes</button></div>`;
    default:
      return false;
  }
}
