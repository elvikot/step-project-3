import { elementsForm } from "./variables.js";
import { Modal } from "./helpers.js";
import { Request } from "./request.js";
import { cardRender } from "./functions.js";

class ElementForm {
    constructor(tagName) {
        this.tagName = tagName;
    }
}

class Input extends ElementForm {
    constructor(tagName, { name, type, placeholder, label, className = 'input-base', required = 'required', dataMax = '', dataMin = '1900-01-01' }){
        super(...arguments);
        this.name = name;
        this.type = type;
        this.placeholder = placeholder;
        this.label = label;
        this.className = className;
        this.dataMax = dataMax;
        this.dataMin = dataMin;
        this.required = required;
    }

    createInput() {
        this.input = document.createElement(`${this.tagName}`);
        this.input.type = this.type;
        this.input.dataset.name = this.name;
        this.input.placeholder = this.placeholder;
        this.input.classList.add(`${this.className}`);
        this.input.required = this.required;
        if (this.name === 'lastVisitDate') {
            this.input.max = this.dateNow();
            this.input.min = this.dataMin;
            this.input.style.minWidth = '1%'
        }
        return this.putInputInLabel();
    }

    putInputInLabel() {
        const label = document.createElement('label');
        label.textContent = this.label;
        label.classList.add('label-visit');
        label.classList.add('base');
        label.append(this.input);
        const inputErr = document.createElement('div');
        inputErr.classList.add('input-error', 'hidden');
        label.insertAdjacentElement('beforeend', inputErr);
        return label;
    }

    dateNow() {
        const now = new Date();
        let day = now.getDate().toString().padStart(2, '0');
        let month = (now.getMonth() + 1).toString().padStart(2, '0');
        const year = now.getFullYear();
        return `${year}-${month}-${day}`;
    }
}

class Select extends ElementForm {
    constructor( tagName, { name, title, options, className = 'select-base', required = 'required' }){
        super(...arguments);
        this.name = name;
        this.title = title;
        this.options = options;
        this.className = className;
        this.required = required;
    }

    creaeteSelect() {
        this.select = document.createElement(`${this.tagName}`);
        this.select.classList.add(`${this.className}`);
        this.select.classList.add('select-value');
        this.select.dataset.name = this.name;
        this.select.required = this.required;
        this.createOptions();
        return this.createSelectDescr();

    }

    createOptions() {
        const firstOption = document.createElement('option');
        firstOption.value = '';
        firstOption.textContent = this.options[0];
        firstOption.disabled = true;
        firstOption.selected = true;
        const descrOption = this.options.shift();
        const fragment = document.createDocumentFragment();
        fragment.appendChild(firstOption);
        this.options.forEach(value => {
          const option = document.createElement('option');
          option.value = value;
          option.textContent = value;
          fragment.appendChild(option);
        });
        this.options.unshift(descrOption);
        this.select.appendChild(fragment);
      }
      
    createSelectDescr() {
        const selectDescr = document.createElement('div');
        selectDescr.textContent = this.title;
        selectDescr.append(this.select);
        return selectDescr;
    }

}

class Button extends ElementForm {
    constructor( tagName, { name, type, textContent, className = 'btn-submit' }){
        super(...arguments);
        this.name = name;
        this.type = type;
        this.textContent = textContent;
        this.className = className;
    }
    createButton() {
        this.button = document.createElement(`${this.tagName}`);
        this.button.textContent = this.textContent;
        this.button.type = this.type;
        this.button.classList.add(`${this.className}`);
        return this.button
    }
}

class Form {
    static createElement(obj, form) {
        switch (obj.tagName) {
            case 'select': {
                const baseSelect = new Select('select', obj);
                const baseSelectHtml = baseSelect.creaeteSelect();
                if (obj.name === 'doctor') {
                    form.append(baseSelectHtml);
                } else {
                    form.insertBefore(baseSelectHtml, form.lastChild);
                }
                break;
            }
            case 'input': {
                const baseInput = new Input('input', obj);
                const baseInputHtml = baseInput.createInput();
                form.insertBefore(baseInputHtml, form.lastChild);
                break;
            }
            case 'button': {
                const baseButton = new Button('button', obj);
                const baseButtonHtml = baseButton.createButton();
                form.append(baseButtonHtml);
                break;
            }
        }
    }

    static validateFormInputs(form) {
        let isValid = true;
        const inputs = form.querySelectorAll('input');
        const inputValidations = {
            age: [/^(1[8-9]|[2-9]\d)$/, 'Enter correct age'],
            patientName: [/^[a-zA-Z]+\s[a-zA-Z]+$/, 'Enter first name and last name'],
            arterialPressure: [/^\d{2,3}\/\d{2,3}$/, 'Enter your pressure like 120/80'],
            bmi: [/^\d+(\.\d+)?\/\d+(\.\d+)?$/, 'Enter like example: 64/1.71']
        };
        inputs.forEach(input => {
          const inputValue = input.value;
          const inputErr = input.nextElementSibling;
          const validation = inputValidations[input.dataset.name];
            if (validation && !validation[0].test(inputValue)) {
                input.style.color = 'red';
                inputErr.classList.remove('hidden');
                inputErr.textContent = validation[1];
                isValid = false;
            } else {
                input.style.color = 'black';
                inputErr.classList.add('hidden');
            }
        })
        return isValid
    }
}

export class LogInForm extends Form {
    createBaseForm() {
        this.form = document.createElement('form');
        const logInInputs = elementsForm.filter(obj => obj.specialization.find(e => e === 'log-in'));
        const btnSubmitObj = elementsForm.filter(obj => obj.specialization.find(e => e === 'logInForm'))[0];
        Form.createElement(btnSubmitObj, this.form)
        logInInputs.forEach(obj => {
            Form.createElement(obj, this.form);
        })
        const modal = new Modal('', this.form, 'modal__login', '');
        modal.createModal();
    }
}

export class VisitForm extends Form{
    createBaseVisitForm() {
        this.form = document.createElement('form');
        const selectWichDoctor = elementsForm.find(obj => obj.name === 'doctor');
        const btnSubmitObj = elementsForm.filter(obj => obj.specialization.find(e => e === 'visitForm'))[0];
        const select = new Select('select', selectWichDoctor);
        const selectDiv = select.creaeteSelect();
        selectDiv.classList.add('doctor-choose');
        this.form.append(selectDiv);
        Form.createElement(btnSubmitObj, this.form);
        const modal = new Modal('Please, enter the information below to register a visit', this.form, 'modal-create-visit');
        modal.createModal();
        this.chooseDoctor();
    }

    chooseDoctor() {
        const selectDoctor = this.form.querySelector('.select-value');
        selectDoctor.addEventListener('change', e => {
            const allInputs = this.form.querySelectorAll('.base');
            allInputs.forEach(input => input.remove());
            const doctor = selectDoctor.value;
            this.createBaseInputs();
            this.createSpecialInputs(doctor);
        })
    }

    createSpecialInputs(specialization) {
        const inputs = elementsForm.filter(obj => obj.specialization.find(e => e === specialization));
        inputs.forEach(obj => {
            Form.createElement(obj, this.form);
        })
    }

    createBaseInputs() {
        const baseInputs = elementsForm.filter(obj => obj.specialization.find(e => e === 'base'));
        baseInputs.forEach(obj => {
            const element = obj.tagName === 'select' ? new Select('select', obj).creaeteSelect() : new Input('input', obj).createInput();
            if(element.tagName === 'DIV') {
                element.classList.add('urgency', 'base');
            }
            this.form.insertBefore(element, this.form.lastChild);
        })
    }
    collectData() {
        this.form.addEventListener('submit', ev => {
            ev.preventDefault();
            const arrKeyValue = [{status: 'open'}];
            const formFields = this.form.querySelectorAll('[data-name]');
            formFields.forEach(field => {
                const { dataset: { name }, value } = field;
                arrKeyValue.push({ [name]: value });
                field.addEventListener('focus', ev => field.style.color = 'black');
            });
            if (Form.validateFormInputs(this.form)) {
                const dataObj = arrKeyValue.reduce((accumulator, currentValue) => ({ ...accumulator, ...currentValue }));
                this.form.closest('.modal-wrapper').remove();
                Request.createCard(dataObj).then(data => {
                    cardRender(data)});
            }
        })
    }
}