import { elementsForm } from "../variables.js";
import { Modal } from "../modal.js";
import { Request } from "../request.js";
import { cardRender } from "../functions.js";
import { Form } from "./form.js";
import { Select } from "./select.js";
import { Input } from "./input.js";

export class VisitForm extends Form {
  createBaseVisitForm() {
    this.form = document.createElement("form");
    const selectWichDoctor = elementsForm.find((obj) => obj.name === "doctor");
    const btnSubmitObj = elementsForm.filter((obj) =>
      obj.specialization.find((e) => e === "visitForm")
    )[0];
    const select = new Select("select", selectWichDoctor);
    const selectDiv = select.creaeteSelect();
    selectDiv.classList.add("doctor-choose");
    this.form.append(selectDiv);
    Form.createElement(btnSubmitObj, this.form);
    const modal = new Modal(
      "Please, enter the information below to register a visit",
      this.form,
      "modal-create-visit"
    );
    modal.createModal();
    this.chooseDoctor();
  }

  chooseDoctor() {
    const selectDoctor = this.form.querySelector(".select-value");
    selectDoctor.addEventListener("change", (e) => {
      const allInputs = this.form.querySelectorAll(".base");
      allInputs.forEach((input) => input.remove());
      const doctor = selectDoctor.value;
      this.createBaseInputs();
      this.createSpecialInputs(doctor);
    });
  }

  createSpecialInputs(specialization) {
    const inputs = elementsForm.filter((obj) =>
      obj.specialization.find((e) => e === specialization)
    );
    inputs.forEach((obj) => {
      Form.createElement(obj, this.form);
    });
  }

  createBaseInputs() {
    const baseInputs = elementsForm.filter((obj) =>
      obj.specialization.find((e) => e === "base")
    );
    baseInputs.forEach((obj) => {
      const element =
        obj.tagName === "select"
          ? new Select("select", obj).creaeteSelect()
          : new Input("input", obj).createInput();
      if (element.tagName === "DIV") {
        element.classList.add("urgency", "base");
      }
      this.form.insertBefore(element, this.form.lastChild);
    });
  }
  collectData() {
    this.form.addEventListener("submit", (ev) => {
      ev.preventDefault();
      const arrKeyValue = [{ status: "open" }];
      const formFields = this.form.querySelectorAll("[data-name]");
      formFields.forEach((field) => {
        const {
          dataset: { name },
          value,
        } = field;
        arrKeyValue.push({ [name]: value });
        field.addEventListener("focus", (ev) => (field.style.color = "black"));
      });
      if (Form.validateFormInputs(this.form)) {
        const dataObj = arrKeyValue.reduce((accumulator, currentValue) => ({
          ...accumulator,
          ...currentValue,
        }));
        this.form.closest(".modal-wrapper").remove();
        Request.createCard(dataObj).then((data) => {
          cardRender(data);
        });
      }
    });
  }
}
