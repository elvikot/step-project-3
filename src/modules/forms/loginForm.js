import { elementsForm } from "../variables.js";
import { Modal } from "../modal.js";
import { Form } from "./form.js"

export class LogInForm extends Form {
    createBaseForm() {
        this.form = document.createElement('form');
        const logInInputs = elementsForm.filter(obj => obj.specialization.find(e => e === 'log-in'));
        const btnSubmitObj = elementsForm.filter(obj => obj.specialization.find(e => e === 'logInForm'))[0];
        Form.createElement(btnSubmitObj, this.form)
        logInInputs.forEach(obj => {
            Form.createElement(obj, this.form);
        })
        const modal = new Modal('', this.form, 'modal__login', '');
        modal.createModal();
    }
}