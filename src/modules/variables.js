export const addVisitBtn = document.querySelector('.btn-add');
export const logInBtn = document.querySelector('.btn-login');
export const logOutBtn = document.querySelector('.btn-logout');

export const url = 'https://ajax.test-danit.com/api/v2/cards/login';

export const mainIn = document.querySelector('.main-in');
export const mainOut = document.querySelector('.main-out');
export const footer = document.querySelector('.all-rights');
// export const templateEmail = 'maksim.guk@gmail.com';
// export const templatePassword = 'Kotyky';

export const visitsContainer = document.querySelector('.list-visits');
export const visitsList = document.querySelector('.list-visits-container');


export const elementsForm = [
    {
        name: 'doctor',
        tagName: 'select',
        title: 'Сhoose a doctor:',
        options: ['Type of doctor', 'cardiologist', 'dentist', 'therapist'],
        specialization: ['none']
    },
    {
        name: 'title',
        tagName: 'input',
        type: 'text',
        placeholder: 'Short info about purpose',
        label: 'Purpose of the visit:',
        specialization: ['base']
    },
    {
        name: 'description',
        tagName: 'input',
        type: 'text',
        placeholder: 'Several words about your problem',
        label: 'Short description:',
        specialization: ['base']
    },
    {
        name: 'urgency',
        tagName: 'select',
        title: 'Urgency:',
        options: ['Degree of urgency', 'low', 'normal', 'high'],
        specialization: ['base']
    },
    {
        name: 'patientName',
        tagName: 'input',
        type: 'text',
        placeholder: 'enter your first and last name',
        label: 'Full name:',
        specialization: ['base']
    },
    {
        name: 'lastVisitDate',
        tagName: 'input',
        type: 'date',
        placeholder: 'dd/mm/yyyy',
        label: 'Date of last visit',
        specialization: ['dentist'],
        dataMax: '',
        dataMin: '1900-01-01'
    },
    {
        name: 'age',
        tagName: 'input',
        type: 'text',
        placeholder: '18-99',
        label: 'Age:',
        specialization: ['cardiologist', 'therapist']
    },
    {
        name: 'arterialPressure',
        tagName: 'input',
        type: 'text',
        placeholder: '120/80',
        label: 'Arterial pressure:',
        specialization: ['cardiologist']
    },
    {
        name: 'bmi',
        tagName: 'input',
        type: 'text',
        placeholder: 'weight(kg)/height(m)',
        label: 'Body mass index:',
        specialization: ['cardiologist']
    },
    {
        name: 'illnesses',
        tagName: 'input',
        type: 'text',
        placeholder: 'Enter your transferred diseases',
        label: 'Transferred diseases of the cardiovascular system:',
        specialization: ['cardiologist']
    },
    {
        name: 'login__email',
        tagName: 'input',
        type: 'email',
        placeholder: 'example@gmail.com',
        label: 'Login:',
        specialization: ['log-in'],
        className: 'login__email'
    },
    {
        name: 'login__password',
        tagName: 'input',
        type: 'password',
        placeholder: 'password',
        label: 'Password:',
        specialization: ['log-in'],
        className: 'login__password'
    },
    {
        name: 'btn-submit',
        tagName: 'button',
        type: 'submit',
        textContent: 'Create new visit',
        specialization: ['visitForm'],
        className: 'btn-submit'
    },
    {
        name: 'btn-submit',
        tagName: 'button',
        type: 'submit',
        textContent: 'Log In',
        specialization: ['logInForm'],
        className: 'login__submit'
    }
]

export const formFilter = document.querySelector('.filter');
export const titleInput = document.querySelector('.search');
export const statusInput = document.querySelector('.visit-status');
export const urgencyInput = document.querySelector('.visit-urgency');

