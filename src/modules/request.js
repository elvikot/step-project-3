import {getToken} from "./functions.js";

export class Request {
  constructor() {
  }

  static getAllCards() {
    return fetch(`https://ajax.test-danit.com/api/v2/cards`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${getToken()}`
      },
    })
      .then(response => response.json())
  }

  static getCard(id) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${getToken()}`
      },
    })
      .then(response => response.json())
  }
  static deleteCard(id) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${getToken()}`
      },
    })
      .then(response => console.log(response))
  }

  static createCard(body) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${getToken()}`
      },
      body: JSON.stringify(body)
    })
      .then(response => response.json())
  }

  static editCard(id, requestBody) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${getToken()}`
      },
      body: JSON.stringify(requestBody)
    })
      .then(response => response.json())
  }
}
